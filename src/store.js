import {createStore,applyMiddleware} from "redux";
import {logger} from "redux-logger/src";
import Reducer from "./Reducer/Reducer";

const store = createStore(Reducer,applyMiddleware(logger))

export default store
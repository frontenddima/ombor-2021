import {connect} from "react-redux";
import {Link} from 'react-router-dom'
import {useState, useEffect} from "react";

import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import ListAltIcon from '@material-ui/icons/ListAlt';
function StorageResidue(props) {
    const [page, setPage] = useState(1)
    const [paginationP, setPaginationP] = useState(true)
    const [paginationN, setPaginationN] = useState(true)
    console.log(page)

    function paginationPrev() {
        if (page > 1) {
            setPaginationP(false)
        } else {
            setPaginationP(true)
        }
    }

    function paginationNext() {
        // alert(typeof parseInt(props.state.length / 10 + 1))
        // alert(typeof page)
        if ((props.state.length / 10 + 1) > page) {
            setPaginationN(false)
        } else {
            setPaginationN(true)
        }
    }

    useEffect(() => {
        paginationPrev()
    }, [page])

    useEffect(() => {
        paginationNext()
    }, [page])

    return <div className={'container'}>
        <div className="row">
            <div className="col-md-12">
                <h4 className={'text-center'}>Maxsulotlar ombori</h4>
                <h5 className={'text-right excel'}> <ListAltIcon/> Excel</h5>
                <table className={'table table-hover table_style'}>
                    <thead className={'text-center'}>
                    <tr className={'table-primary'}>
                        <th>N</th>
                        <th>Taminotchi</th>
                        <th>Yo'nalish nomi</th>
                        <th>Maxsulot nomi</th>
                        <th>Maxsulot miqdori</th>
                        <th>Maxsulot kelgan sana</th>
                        <th>Chiqim</th>
                    </tr>
                    </thead>
                    <tbody className={'text-center'}>
                    {
                        props.state.filter(item => item.id > (page - 1) * 10 && item.id <= page * 10).map(item => <tr
                            key={item.id}>
                            <td>{item.id}</td>
                            <td>{item.taminotchi}</td>
                            <td>{item.status}</td>
                            <td>{item.nameProduct}</td>
                            <td>{item.amount}&nbsp;{item.measure}</td>
                            <td>{item.date}</td>
                            <td>
                                <Link to={'/ombor/' + item.status + '/' + item.nameProduct + '/' + item.id}>
                                    <button className={'btn btn-secondary'}>Chiqim</button>
                                </Link>
                            </td>
                        </tr>)
                    }
                    </tbody>
                </table>
            </div>
            <div className="col-md-12">
                <button className={'btn btn-secondary'} onClick={() => setPage(prevState => prevState - 1)}
                        disabled={paginationP}><KeyboardArrowLeftIcon/>
                </button>
                <button className={'btn btn-secondary'} onClick={() => setPage(prevState => prevState + 1)}
                        disabled={paginationN}>
                    <KeyboardArrowRightIcon/>
                </button>
            </div>
        </div>

    </div>
}

function mapStateToProps(state) {
    return {
        state: state.Storage
    }
}

export default connect(mapStateToProps, null)(StorageResidue);
import React from "react";
import {connect} from "react-redux";
import {Link} from 'react-router-dom'

import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function oneProduct(props) {

    const notify = () => toast.info("Maxsulot chiqim qilindi!");

    const product = props.match.params

    function ChiqimSaqlash(e) {
        e.preventDefault()
        let username = e.target[0].value;
        let chiqStatus = e.target[1].value;
        let chiNameProduct = e.target[2].value;
        let chiqAmount = e.target[3].value;
        let chiqMeasure = e.target[4].value;
        let chiqDate = e.target[5].value;

        if (username && chiqStatus && chiNameProduct && chiqAmount && chiqMeasure && chiqDate) {
            props.ChiqFormValue({
                id: props.state.length + 1,
                chiqStatus,
                chiNameProduct,
                chiqAmount,
                chiqMeasure,
                chiqDate
            })
            notify()
        }

        if (username && chiqStatus && chiNameProduct && chiqAmount && chiqMeasure && chiqDate) {
            props.ChiqFormValueChange({
                id: product.id,
                chiqAmount,
            })
        }
    }


    return <div className={'container'}>
        <h4 className={'text-center mb-3'}>Yo'nalishlarga chiqim qilish </h4>
        <div className="row">
            <div className="col-md-6 offset-3 oneProduct_style">
                <form onSubmit={ChiqimSaqlash} id={'saqlash'} className={'form_style'}>
                    <label htmlFor="user" className={'d-block'}>Username
                        <input type="text" className={'form-control'} id={'user'} placeholder={'Username'}/>
                    </label><br/>
                    <label htmlFor="yonalish" className={'d-block'}>Yo'nalish
                        <select className={'form-control w-100'} id={'yonalish'}>
                            <option>{product.status}</option>
                        </select>
                    </label><br/>
                    <label htmlFor="nomi" className={'d-block'}>Maxsulot nomi
                        <input type="text" className={'form-control'} defaultValue={product.nameProduct} id={'nomi'}/>
                    </label>
                    <div className={'d-flex'}>
                        <label htmlFor="miqdor">Maxsulot miqdori
                            <input type="text" className={'form-control'} placeholder={'Maxsulot miqdori'}
                                   id={'miqdor'}/>
                        </label>
                        <label htmlFor="olchov" className={'mx-4'}>O'lchov
                            <select className={'form-control'} id={'olchov'}>
                                <option>{''}</option>
                                <option>шт</option>
                                <option>т</option>
                                <option>кг</option>
                                <option>гр</option>
                                <option>км</option>
                                <option>м</option>
                                <option>см</option>
                            </select>
                        </label>
                    </div>
                    <label htmlFor="date" className={'d-block'}>Chiqim sanasi
                        <input type="date" className={'form-control'} id={'date'}/>
                    </label>
                    <div className={'d-flex'}>
                        <button className={'btn btn-success'} form={'saqlash'}>Saqlash</button>
                        <Link to={'/'}>
                            <button className={'btn btn-secondary mx-3'}>Chiqish</button>
                        </Link>
                    </div>
                </form>
            </div>
        </div>
        <ToastContainer />
    </div>
}

function mapStateToProps(state) {
    return {
        state: state.chiqim,
        globalState: state.Storage
    }
}

function mapDispatchToProps(dispatch) {
    return {
        ChiqFormValue: (ChiqValue) => {
            dispatch({type: 'SET_DATA_CHIQIM', payload: ChiqValue})
        },
        ChiqFormValueChange: (changeValue) => {
            dispatch({type: 'SET_DATA_CHANGE', payload: changeValue})
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(oneProduct);
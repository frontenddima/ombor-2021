import React from 'react';
import {connect} from "react-redux";
import './App.css'

import {makeStyles} from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';

import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import AccountTreeIcon from '@material-ui/icons/AccountTree';
import StorageIcon from '@material-ui/icons/Storage';
// import logo from 'src/monocenter_notblack.png'

import StorageResidue from "./component/StorageResidue";
import oneProduct from "./component/oneProduct";
import {Route, Link, Switch} from 'react-router-dom'
import Hisobot from "./Pages/Hisobot";

import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function rand() {
    return Math.round(10) - 10;
}

function getModalStyle() {
    const top = 50 + rand();
    const left = 50 + rand();

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}

const useStyles = makeStyles((theme) => ({
    paper: {
        position: 'absolute',
        width: 400,
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}));


function App(props) {

    const notify = () => toast.success("Maxsulot omborga saqlandi!");

    const classes = useStyles();
    // getModalStyle is not a pure function, we roll the style only on the first render
    const [modalStyle] = React.useState(getModalStyle);
    const [open, setOpen] = React.useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const body = (
        <div style={modalStyle} className={classes.paper}>
            <h4 id="simple-modal-title">Maxsulotlarni kirim qilish</h4>
            <form onSubmit={saqlash} id={'saqlash'}>
                <label htmlFor="tashkilot">Taminotchi nomi
                    <input type="text" className={'form-control'} placeholder={'Taminotchi nomi'} id={'tashkilot'}/>
                </label>
                <label htmlFor="yonalish">Yo'nalish
                    <select className={'form-control'} id={'yonalish'}>
                        <option>...</option>
                        <option>IT va Design</option>
                        <option>Sartaroshlik</option>
                        <option>Pazandachilik</option>
                    </select>
                </label>
                <label htmlFor="nomi">Maxsulot nomi
                    <input type="text" className={'form-control'} placeholder={'Maxsulot nomi'} id={'nomi'}/>
                </label>
                <div className={'d-flex'}>
                    <label htmlFor="miqdor">Maxsulot miqdori
                        <input type="text" className={'form-control'} placeholder={'Maxsulot miqdori'} id={'miqdor'}/>
                    </label>
                    <label htmlFor="olchov" className={'mx-4'}>O'lchov
                        <select className={'form-control'} id={'olchov'}>
                            <option>...</option>
                            <option>шт</option>
                            <option>т</option>
                            <option>кг</option>
                            <option>гр</option>
                            <option>км</option>
                            <option>м</option>
                            <option>см</option>
                        </select>
                    </label>
                </div>
                <label htmlFor="date">Sana
                    <input type="date" className={'form-control'} id={'date'}/>
                </label>
                <div className={'d-flex'}>
                    <button className={'btn btn-success'} form={'saqlash'}>Saqlash</button>
                    <button className={'btn btn-secondary mx-3'} onClick={handleClose}>Chiqish</button>
                </div>
            </form>
        </div>
    );


    function saqlash(e) {
        e.preventDefault()
        let taminotchi = e.target[0].value;
        let status = e.target[1].value;
        let nameProduct = e.target[2].value;
        let amount = e.target[3].value;
        let measure = e.target[4].value;
        let date = e.target[5].value;

        if (taminotchi && status && nameProduct && amount && measure && date) {
            props.formValue({
                id: props.state.length + 1,
                taminotchi,
                status,
                nameProduct,
                amount,
                measure,
                date
            })
            notify()
        }
        handleClose()
    }

    return (
        <div>
            <section className={'mt-2 menu'}>
                <div>
                    {/*<img src={logo}/>*/}
                    <h2>Logo</h2>
                </div>
                <div className={'menu_right'}>
                    <ul>
                        <li><AccountTreeIcon/>
                            <Link to={'./hisobot'}> Hisobot</Link>
                        </li>
                        <li><StorageIcon/>
                            <Link to={'./ombor'}> Ombor</Link>
                        </li>
                        <li onClick={handleOpen}><AddShoppingCartIcon/> Kirim qilish</li>
                    </ul>
                </div>
            </section>

            <section>
                <Switch>
                    <Route path={'/ombor/:status/:nameProduct/:id'} component={oneProduct}/>
                    <Route path={'/ombor'} component={StorageResidue}/>
                    <Route path={'/ombor'} component={App}/>
                    <Route path={'/hisobot'} component={Hisobot}/>
                </Switch>
            </section>

            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
            >
                {body}
            </Modal>

            <ToastContainer/>

        </div>


    );
}

function mapDispatchToProps(dispatch) {
    return {
        formValue: (value) => {
            dispatch({type: 'SET_DATA', payload: value})
        }
    }
}

function mapStateToProps(state) {
    return {
        state: state.Storage
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(App);

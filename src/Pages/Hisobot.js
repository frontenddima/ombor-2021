import {connect} from "react-redux";
import PrintIcon from '@material-ui/icons/Print';

function Hisobot(props) {

    return <div className={'container'}>
        <div className={'row'}>
            <div className="col-md-12">
                <form className={'input_data'}>
                    <div>
                        <label htmlFor="dan">dan:
                            <input type="date" className={'form-control'} id={'dan'}/>
                        </label>
                        <label htmlFor="gacha" className={'mx-3'}>gacha:
                            <input type="date" className={'form-control'} id={'gacha'}/>
                        </label>
                        <label htmlFor="qidiruv" className={'mx-3 border_none_search'}>Qidiruv
                            <input type="text" className={'form-control'} id={'qidiruv'} placeholder={'Qidiruv'}/>
                        </label>
                    </div>
                    <div className={'print d-print-table'}>
                        <h6> <PrintIcon/> Chop etish</h6>
                    </div>
                </form>
            </div>
            <div className={'col-md-12'}>
                <table className={'table table-hover text-center table_style'}>
                    <thead>
                    <tr className={'table-primary'}>
                        <th>№</th>
                        <th>Hamkor tashkilot</th>
                        <th>Maxsulot nomi</th>
                        <th>Oldingi qoldiq</th>
                        <th>Chiqim</th>
                        <th>Keyingi qoldiq</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        props.StorageState.map(item => <tr key={item.id}>
                            <td>{item.id}</td>
                            <td>{item.taminotchi}</td>
                            <td>{item.nameProduct}</td>
                            <td>
                                {
                                    parseInt(props.chiqimState.map(element => (element.chiNameProduct === item.nameProduct) ? (element.chiqAmount) : 0)) + parseInt(item.amount) + ' ' + item.measure
                                }
                            </td>
                            <td>
                                {
                                    props.chiqimState.map(element => (element.chiNameProduct === item.nameProduct) ? (element.chiqAmount + ' ' + element.chiqMeasure) : '')
                                }
                            </td>
                            <td>{item.amount}&nbsp;{item.measure}</td>
                        </tr>)
                    }
                    </tbody>
                </table>
            </div>
        </div>
    </div>
}

function mapStateToProps(state) {
    return {
        StorageState: state.Storage,
        chiqimState: state.chiqim
    }
}

export default connect(mapStateToProps, null)(Hisobot);
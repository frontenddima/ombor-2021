export default function Reducer(state = {
    Storage: [],
    chiqim: []
}, action) {

    switch (action.type) {
        case 'SET_DATA':
            let Storage = [...state.Storage]
            Storage.push(action.payload)
            state = {...state, Storage}
            break;
        case 'SET_DATA_CHANGE':
            let update = state.Storage.map((item) => {
                if (parseInt(action.payload.id) === item.id
                    && parseInt(item.amount) > 0
                    && parseInt(item.amount) >= parseInt(action.payload.chiqAmount)) {
                    item = {
                        ...item,
                        amount: parseInt(item.amount) - parseInt(action.payload.chiqAmount)
                    }
                }
                return item
            })
            state = {
                ...state,
                Storage: update
            }
            break;
        case 'SET_DATA_CHIQIM':
            let chiqim = [...state.chiqim]
            chiqim.push(action.payload)
            state = {...state, chiqim}
            break;
    }
    return state;
}